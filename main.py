import re
from termcolor import colored

logfile = "/var/log/syslog"   

with open(logfile, "r") as f:
    for line in f:
        if re.search("error", line, re.IGNORECASE):
            print(colored(line.strip(), "red"))
        elif re.search("failure", line, re.IGNORECASE):
            print(colored(line.strip(), "red"))
        elif re.search("warning", line, re.IGNORECASE):
            print(colored(line.strip(), "yellow"))
